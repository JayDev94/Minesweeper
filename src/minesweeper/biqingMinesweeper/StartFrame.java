/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

/**
 *
 * @author josh
 */
public class StartFrame extends JFrame implements ActionListener {

    //Menu objects
    JMenuBar menuBar;
    JMenu menu;
    JMenuItem menuItem;
    JRadioButtonMenuItem rbMenuItem;
    JCheckBoxMenuItem cbMenuItem;
    JFrame frame;

    //Card layout
    CardLayout cardLayout = new CardLayout();
    JPanel cardPanel;
    StartPanel startPanel;
    GamePlayPanel playPanel;
    String cardName = "1";

    String difficultySelection = "Easy";

    public StartFrame() {
        //Initialize the menu
        gameMenu();

        //JPanel swtich settings useing CardLayout
        cardPanel = new JPanel();
        cardPanel.setLayout(cardLayout);

        //Send this frame to startPanel 
        frame = this;
        startPanel = new StartPanel(frame);
        playPanel = new GamePlayPanel(difficultySelection);
        cardPanel.add(startPanel, "0");
        cardPanel.add(playPanel, "1");
        //Show the start panal by default
        cardLayout.show(cardPanel, "0");

        //On difficulty button called in startPanel for change in cards
        startPanel.difficultyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] difficultyList = new String[3];
                difficultyList[0] = "Easy";
                difficultyList[1] = "Medium";
                difficultyList[2] = "Hard";
                JOptionPane optionPane = new JOptionPane();
                String selection = (String) optionPane.showInputDialog(frame, "Pick a Difficulty", "Input", JOptionPane.QUESTION_MESSAGE, null, difficultyList, "Titan");

                switch (selection) {
                    case "Easy":
                        difficultySelection = "Easy";
                        break;
                    case "Medium":
                        difficultySelection = "Medium";
                        break;
                    case "Hard":
                        difficultySelection = "Hard";
                        break;
                    default:
                        difficultySelection = "Easy";
                }
                startPanel.difficultyButton.setText("Difficulty: " + difficultySelection);
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

        //on start button called in startPanel for change in cards (JPanels)
        startPanel.startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playPanel = new GamePlayPanel(difficultySelection);
                cardPanel.add(playPanel, "1");
                cardPanel.revalidate();
                cardLayout.show(cardPanel, "1");
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

  

        frame.add(cardPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(800, 800));
        frame.setResizable(false);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        //default game difficulty
        String difficultyString = "Easy";

        /* //create start frame
        StartFrame startFrame = new StartFrame();
        startFrame.setSize(new Dimension(600, 700));
        startFrame.setResizable(false);
        startFrame.setLayout(new BorderLayout());

        StartPanel startPanel = new StartPanel(startFrame);
        startFrame.add(startPanel);*/

 /*  start.add(startPanel.getTitlePanel(), BorderLayout.CENTER);
        start.add(startPanel.buttonPanel, BorderLayout.SOUTH);
        start.add(startPanel.leftPanel, BorderLayout.WEST);
        start.add(startPanel.rightPanel, BorderLayout.EAST);
        start.add(startPanel.topPanel, BorderLayout.NORTH);
        startFrame.pack();
        startFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        startFrame.setVisible(true);*/
        //New StartFrame
        StartFrame startFrame = new StartFrame();

    }

    public void getDifficulty(int difficultyInt) {

    }

    public void gameMenu() {
        //Menu bar
        menuBar = new JMenuBar();
        //Menu build
        menu = new JMenu("MENU");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription("Something blah blah");
        menuBar.add(menu);

        //Menu items build
        menuItem = new JMenuItem("HOME", KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, ActionEvent.ALT_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription("Wow, such menu");
        //Add a listener to the item
        menuItem.addActionListener(this);
        menu.add(menuItem);

        menuBar.add(menu);
        this.setJMenuBar(menuBar);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        cardLayout.show(cardPanel, "0");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
