/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;


public class Stopwatch {

    /**
     * Whether the stopwatch is running
     */
    private boolean running;

    /**
     * Whether the Stopwatch has been paused... not actively counting but the
     * start time should be preserved
     */
    private boolean paused;

    /**
     * The start time in microseconds
     */
    private long start;

    /**
     * The Start time for the current paused time
     */
    private long pausedStart;

    /**
     * The end time
     */
    private long end;

 

    public Stopwatch() {
        start=System.nanoTime();
        
        paused=false;
        running=false;
        
    }

    
    public boolean isRunning() {
        return running;
    }

   
    public boolean isPaused() {
        return paused;
    }

   
    public void start() {
        start = System.nanoTime();
        running = true;
        paused = false;
        pausedStart = -1;
    }

   
    public long stop() {
        if (!isRunning()) {
            return -1;
        } else if (isPaused()) {
            running = false;
            paused = false;

            return pausedStart - start;
        } else {
            end = System.nanoTime();
            running = false;
            return end - start;
        }
    }

   
    public long elapsed() {
        if (isRunning()) {
            System.out.println("is running, start time: "+start + ", current time: "+ System.nanoTime() + ", elapsed time: " + (System.nanoTime()-start));
            if (isPaused()) {
                return (pausedStart - start);
            }
            return (System.nanoTime() - start);
        } else {
            return (end - start);
        }
    }

    public String toString() {
        long enlapsed = elapsed();
        return ((double) enlapsed / 1000000000.0) + " Seconds";
    }
    
    
    /*   public long pause() {
        if (!isRunning()) {
            return -1;
        } else if (isPaused()) {
            return (pausedStart - start);
        } else {
            pausedStart = System.nanoTime();
            paused = true;
            return (pausedStart - start);
        }
    }

   
    public void resume() {
        if (isPaused() && isRunning()) {
            start = System.nanoTime() - (pausedStart - start);
            paused = false;
        }
    }
*/

}
