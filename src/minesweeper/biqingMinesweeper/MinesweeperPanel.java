package minesweeper.biqingMinesweeper;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MinesweeperPanel extends JPanel implements ActionListener, MouseListener {

    // create frame for the Minesweeper
    //JFrame frame = new JFrame("Minesweeper");
    // create reset button
    //JButton reset = new JButton("Reset");
    // 10 by 10 dimensions of grids
    JButton[][] buttons;
    // added panel for number of mines and time
    //JPanel panel = new JPanel();
    //JTextField mineNumber = new JTextField(3);
    //JTextField timeCost = new JTextField(3);
    // the number showed on each cell
    int[][] counts;

    //designate number to represent a mine in the grid
    final int MINE = 10;
    int width;
    int height;
    boolean gameStarted;
    boolean gameEnded;
    Stopwatch stopwatch;
    int numberOfMines;
    int minesLeft;
    boolean won;

    // Constructor
    public MinesweeperPanel(String difficultyString) {

        // frame.setSize(300, 400);
        //frame.setLayout(new BorderLayout());
        //frame.add(reset, BorderLayout.NORTH);
        //panel.setLayout(new BorderLayout());
        //panel.add(mineNumber, BorderLayout.WEST);
        //panel.add(timeCost, BorderLayout.EAST);
        //frame.getContentPane().add(panel, BorderLayout.SOUTH);
        //reset.addActionListener(this);
        stopwatch = new Stopwatch();
        gameStarted = false;
        gameEnded = false;
        won = false;
        switch (difficultyString) {
            case "Easy":
                width = 10;
                height = 10;
                numberOfMines = 10;
                minesLeft = 10;
                break;
            case "Medium":
                width = 20;
                height = 20;
                numberOfMines = 40;
                minesLeft = 40;
                break;
            case "Hard":
                width = 30;
                height = 30;
                numberOfMines = 90;
                minesLeft = 90;
                break;
            default:
                width = 10;
                height = 10;
                numberOfMines = 10;
                minesLeft = 10;
        }

        buttons = new JButton[width][height];
        counts = new int[width][height];

        this.setLayout(new GridLayout(width, height));

        for (int a = 0; a < buttons.length; a++) {
            for (int b = 0; b < buttons[0].length; b++) {
                buttons[a][b] = new JButton();

                buttons[a][b].addActionListener(this);
                buttons[a][b].addMouseListener(this);
                buttons[a][b].setBackground(Color.WHITE);
                this.add(buttons[a][b]);
            }
        }

        createRandomMines();

    }

    // create mines on the board
    public void createRandomMines() {
        // Initialize list of random pairs
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int x = 0; x < counts.length; x++) {
            for (int y = 0; y < counts[0].length; y++) {
                list.add(x * 100 + y);
            }
        }
        // Reset counts
        counts = new int[width][height];
        // Pick out mines
        for (int a = 0; a < numberOfMines; a++) {
            int choice = (int) (Math.random() * list.size());
            counts[list.get(choice) / 100][list.get(choice) % 100] = MINE;
            list.remove(choice);
        }

        // find the number of mines in the adjacent cells
        for (int x = 0; x < counts.length; x++) {
            for (int y = 0; y < counts[0].length; y++) {
                if (counts[x][y] != MINE) {
                    int neighborcount = 0;
                    if (x > 0 && y > 0 && counts[x - 1][y - 1] == MINE) {
                        neighborcount++;
                    }
                    if (y > 0 && counts[x][y - 1] == MINE) {
                        neighborcount++;
                    }
                    if (x < counts.length - 1 && y > 0 && counts[x + 1][y - 1] == MINE) {
                        neighborcount++;
                    }
                    if (x > 0 && counts[x - 1][y] == MINE) {
                        neighborcount++;
                    }
                    if (x < counts.length - 1 && counts[x + 1][y] == MINE) {
                        neighborcount++;
                    }
                    if (x > 0 && y < counts[0].length - 1 && counts[x - 1][y + 1] == MINE) {
                        neighborcount++;
                    }
                    if (y < counts[0].length - 1 && counts[x][y + 1] == MINE) {
                        neighborcount++;
                    }
                    if (x < counts.length - 1 && y < counts[0].length - 1 && counts[x + 1][y + 1] == MINE) {
                        neighborcount++;
                    }
                    counts[x][y] = neighborcount;
                }
            }
        }

    }

    // open all the adjacent empty cells (no bomb in the adjacent cells) with recursive method
    public void clearZeros(ArrayList<Integer> toClear) {
        if (toClear.size() == 0) {
            return;
        } else {
            int x = toClear.get(0) / 100;
            int y = toClear.get(0) % 100;
            toClear.remove(0);
            if (counts[x][y] == 0) {
                if (x > 0 && y > 0 && buttons[x - 1][y - 1].isEnabled()) {
                    buttons[x - 1][y - 1].setText(counts[x - 1][y - 1] + "");
                    buttons[x - 1][y - 1].setEnabled(false);
                    if (counts[x - 1][y - 1] == 0) {
                        buttons[x - 1][y - 1].setBackground(Color.GREEN);
                        buttons[x - 1][y - 1].setText("");
                        toClear.add((x - 1) * 100 + (y - 1));
                    } else {
                        buttons[x - 1][y - 1].setBackground(Color.ORANGE);
                    }
                }
                if (y > 0 && buttons[x][y - 1].isEnabled()) {
                    buttons[x][y - 1].setText(counts[x][y - 1] + "");
                    buttons[x][y - 1].setEnabled(false);
                    if (counts[x][y - 1] == 0) {
                        buttons[x][y - 1].setBackground(Color.GREEN);
                        buttons[x][y - 1].setText("");
                        toClear.add(x * 100 + (y - 1));
                    } else {
                        buttons[x][y - 1].setBackground(Color.ORANGE);
                    }
                }
                if (x < counts.length - 1 && y > 0 && buttons[x + 1][y - 1].isEnabled()) {
                    buttons[x + 1][y - 1].setText(counts[x + 1][y - 1] + "");
                    buttons[x + 1][y - 1].setEnabled(false);
                    if (counts[x + 1][y - 1] == 0) {
                        buttons[x + 1][y - 1].setBackground(Color.GREEN);
                        buttons[x + 1][y - 1].setText("");
                        toClear.add((x + 1) * 100 + (y - 1));
                    } else {
                        buttons[x + 1][y - 1].setBackground(Color.ORANGE);
                    }
                }
                if (x > 0 && buttons[x - 1][y].isEnabled()) {
                    buttons[x - 1][y].setText(counts[x - 1][y] + "");
                    buttons[x - 1][y].setEnabled(false);
                    if (counts[x - 1][y] == 0) {
                        buttons[x - 1][y].setBackground(Color.GREEN);
                        buttons[x - 1][y].setText("");
                        toClear.add((x - 1) * 100 + y);
                    } else {
                        buttons[x - 1][y].setBackground(Color.ORANGE);
                    }
                }
                if (x < counts.length - 1 && buttons[x + 1][y].isEnabled()) {
                    buttons[x + 1][y].setText(counts[x + 1][y] + "");
                    buttons[x + 1][y].setEnabled(false);
                    if (counts[x + 1][y] == 0) {
                        buttons[x + 1][y].setBackground(Color.GREEN);
                        buttons[x + 1][y].setText("");
                        toClear.add((x + 1) * 100 + y);
                    } else {
                        buttons[x + 1][y].setBackground(Color.ORANGE);
                    }
                }
                if (x > 0 && y < counts[0].length - 1 && buttons[x - 1][y + 1].isEnabled()) {
                    buttons[x - 1][y + 1].setText(counts[x - 1][y + 1] + "");
                    buttons[x - 1][y + 1].setEnabled(false);
                    if (counts[x - 1][y + 1] == 0) {
                        buttons[x - 1][y + 1].setBackground(Color.GREEN);
                        buttons[x - 1][y + 1].setText("");
                        toClear.add((x - 1) * 100 + (y + 1));
                    } else {
                        buttons[x - 1][y + 1].setBackground(Color.ORANGE);
                    }
                }
                if (y < counts[0].length - 1 && buttons[x][y + 1].isEnabled()) {
                    buttons[x][y + 1].setText(counts[x][y + 1] + "");
                    buttons[x][y + 1].setEnabled(false);
                    if (counts[x][y + 1] == 0) {
                        buttons[x][y + 1].setBackground(Color.GREEN);
                        buttons[x][y + 1].setText("");
                        toClear.add(x * 100 + (y + 1));
                    } else {
                        buttons[x][y + 1].setBackground(Color.ORANGE);
                    }
                }
                if (x < counts.length - 1 && y < counts[0].length - 1 && buttons[x + 1][y + 1].isEnabled()) {
                    buttons[x + 1][y + 1].setText(counts[x + 1][y + 1] + "");
                    buttons[x + 1][y + 1].setEnabled(false);
                    if (counts[x + 1][y + 1] == 0) {
                        buttons[x + 1][y + 1].setBackground(Color.GREEN);
                        buttons[x + 1][y + 1].setText("");
                        toClear.add((x + 1) * 100 + (y + 1));
                    } else {
                        buttons[x + 1][y + 1].setBackground(Color.ORANGE);
                    }
                }
            }
            clearZeros(toClear);
        }
    }

    // Action Performed 
    @Override
    public void actionPerformed(ActionEvent event) {
        if (!stopwatch.isRunning()) {
            stopwatch.start();
        }
        gameStarted = true;
        for (int x = 0; x < buttons.length; x++) {
            for (int y = 0; y < buttons[0].length; y++) {
                if (event.getSource().equals(buttons[x][y])) {
                    if (counts[x][y] == MINE) { // click on mine
                        buttons[x][y].setBackground(Color.BLACK);
                        lostGame();
                        gameEnded = true;
                    } else if (counts[x][y] == 0) { // click on empty cell (0 bomb adjacent)
                        buttons[x][y].setBackground(Color.GREEN);
                        buttons[x][y].setEnabled(false);
                        ArrayList<Integer> toClear = new ArrayList<Integer>();
                        toClear.add(x * 100 + y);
                        clearZeros(toClear);
                        checkWin();
                    } else {  // click on a cell with bomb adjacent
                        buttons[x][y].setText(counts[x][y] + "");
                        buttons[x][y].setBackground(Color.ORANGE);
                        buttons[x][y].setEnabled(false);
                        checkWin();
                    }
                }
            }
        }
    }

    //Actions once you clicked on a bomb
    public void lostGame() {
        gameEnded = true;
        for (int x = 0; x < buttons.length; x++) {
            for (int y = 0; y < buttons[0].length; y++) {
                if (buttons[x][y].isEnabled()) {
                    if (counts[x][y] == 0) {
                        buttons[x][y].setBackground(Color.GREEN);
                        buttons[x][y].setEnabled(false);
                    } else if (counts[x][y] < 10 && counts[x][y] > 0) {
                        buttons[x][y].setBackground(Color.ORANGE);
                        buttons[x][y].setText(Integer.toString(counts[x][y]));
                        buttons[x][y].setEnabled(false);
                    } else {
                        buttons[x][y].setBackground(Color.BLACK);
                        buttons[x][y].setText("x");
                        buttons[x][y].setEnabled(false);
                    }
                }
            }
        }

        JOptionPane.showMessageDialog(this.getParent(),"YOU LOST!");
    }

    // check if win the game
    public void checkWin() {
        boolean won = true;
        for (int x = 0; x < counts.length; x++) {
            for (int y = 0; y < counts[0].length; y++) {
                if (counts[x][y] != MINE && buttons[x][y].isEnabled() == true) {
                    won = false;
                }
            }
        }
     
        if (won == true) {
            int endTime = (int) (stopwatch.stop() / 1000000000);
            JOptionPane.showMessageDialog(this.getParent().getParent(), "You finished with a time of " + endTime + "Seconds!");

        }
    }

    // right click to mark the bomb with "M"
    @Override
    public void mouseClicked(MouseEvent e) {
        if (!stopwatch.isRunning()) {
            stopwatch.start();
        }

        gameStarted = true;
        for (int x = 0; x < buttons.length; x++) {
            for (int y = 0; y < buttons[0].length; y++) {
                if (e.getSource() == buttons[x][y] && e.getButton() == MouseEvent.BUTTON3
                        && buttons[x][y].getBackground().equals(java.awt.Color.WHITE)) {
                    buttons[x][y].setBackground(Color.RED);
                    buttons[x][y].setText("M");
                    minesLeft--;
                } else if (e.getSource() == buttons[x][y] && e.getButton() == MouseEvent.BUTTON3 && buttons[x][y].getBackground().equals(java.awt.Color.RED)) {
                    buttons[x][y].setBackground(Color.WHITE);
                    buttons[x][y].setText("");
                    minesLeft++;
                }
            }
        }
    }

    public void resetButtonHit() {
        minesLeft = numberOfMines;
        stopwatch.stop();
        gameStarted = false;
        for (int x = 0; x < buttons.length; x++) {
            for (int y = 0; y < buttons[0].length; y++) {
                buttons[x][y].setEnabled(true);
                buttons[x][y].setText("");
                buttons[x][y].setBackground(Color.WHITE);
            }
        }
        createRandomMines();
    }

    public boolean gameHasEnded() {
        return gameEnded;
    }

    public boolean gameStarted() {
        return gameStarted;
    }

    public String getElapsedToString() {
        int enlapsed = (int) (stopwatch.elapsed() / 1000000000);
        return Integer.toString(enlapsed);
    }

    public String getNumberOfMinesLeft() {
        return Integer.toString(minesLeft);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
