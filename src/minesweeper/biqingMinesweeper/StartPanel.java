/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author josh
 */
public class StartPanel extends JPanel {

    String difficultyString = "Easy";
    JPanel topPanel;
    JPanel leftPanel;
    JPanel rightPanel;
    JPanel titlePanel;
    JPanel buttonPanel;
    JButton startButton;
    JButton difficultyButton;

    public StartPanel(JFrame start) {
        //creating center panel
        topPanel = new JPanel();
        topPanel.setPreferredSize(new Dimension(600, 100));

        leftPanel = new JPanel();
        leftPanel.setPreferredSize(new Dimension(0, 0));

        rightPanel = new JPanel();
        rightPanel.setPreferredSize(new Dimension(0, 0));

        titlePanel = new JPanel();
        titlePanel.setPreferredSize(new Dimension(600, 200));

        JLabel title = new JLabel();
        title.setText("Minesweeper");
        title.setFont(new Font("Helvetica", Font.PLAIN, 70));
        titlePanel.add(title);

        //create start, difficulty, and exit buttons
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(3, 1));
        buttonPanel.setPreferredSize(new Dimension(600, 100));

        startButton = new JButton("Start");
        startButton.setText("Start");
        startButton.setSize(10, 10);
        
        
        
        
        
       /* startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               //GamePlayPanel game = new GamePlayPanel("Easy");
                //gameFrame.setSize(1400, 1000);
                //gameFrame.setResizable(true);
                //gameFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                //gameFrame.setVisible(true);

                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });*/

        difficultyButton = new JButton("Difficulty");
        difficultyButton.setSize(10, 10);
        difficultyButton.setText("Difficulty: " + difficultyString);
        /*difficultyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] difficultyList = new String[3];
                difficultyList[0] = "Easy";
                difficultyList[1] = "Medium";
                difficultyList[2] = "Hard";
                JOptionPane optionPane = new JOptionPane();
                String selection = (String) optionPane.showInputDialog(start, "Pick a Difficulty", "Input", JOptionPane.QUESTION_MESSAGE, null, difficultyList, "Titan");

                switch (selection) {
                    case "Easy":
                        difficultyString = "Easy";
                        break;
                    case "Medium":
                        difficultyString = "Medium";
                        break;
                    case "Hard":
                        difficultyString = "Hard";
                        break;
                    default:
                        difficultyString = "Easy";
                }

                difficultyButton.setText("Difficulty: " + difficultyString);

                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });
*/

        JButton exitButton = new JButton("Exit");
        exitButton.setText("Exit");
        exitButton.setSize(10, 10);
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.exit(0);

                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

        buttonPanel.add(startButton);
        buttonPanel.add(difficultyButton);
        buttonPanel.add(exitButton);

        //Add the panels
        this.add(getTitlePanel(), BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.add(leftPanel, BorderLayout.WEST);
        this.add(rightPanel, BorderLayout.EAST);
        this.add(topPanel, BorderLayout.NORTH);

        this.setPreferredSize(new Dimension(600, 700));
    }

    public JPanel getTitlePanel() {
        return titlePanel;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;

    }

    public JPanel getLeftPanel() {
        return leftPanel;
    }

    public JPanel getRightPanel() {
        return rightPanel;
    }

    public JPanel getTopPanel() {
        return topPanel;
    }

    public String getDifficulty() {
        return difficultyString;
    }

}
