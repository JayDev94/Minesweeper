/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Juan
 */
public class GamePlayPanel extends JPanel {

    //Contains the grid
    MinesweeperPanel msp;
    //Layout

    JLabel nameLabel = new JLabel("Minesweeper");
    JLabel flagLabel = new JLabel("00");
    JPanel clockPanel;
    JLabel stopwatchLabel;
    //reset button
    JButton resetButton;
    JButton exitButton;
    JButton changeDifficultyButton;
    int counter;
    int finishedIn;

    //Time 
    Timer timer;
    int minutes = 0;
    int miliSeconds = 0;
    int seconds = 0;
    String strSec = "00";
    String strMin = "00";
    boolean gameStart = false;

    public GamePlayPanel(String difficultyString) {
        msp = new MinesweeperPanel(difficultyString);
        resetButton = new JButton("RESET");
        resetButton.setForeground(Color.RED);
        //exitButton = new JButton("Home");

        //Time settings
        stopwatchLabel = new JLabel();
        stopwatchLabel.setOpaque(true);
        stopwatchLabel.setBackground(Color.GREEN);
        stopwatchLabel.setFont(new Font("Arial", Font.BOLD, 30));
        stopwatchLabel.setText("00:00");

        JFrame gFrame = (JFrame) this.getParent();

        /*exitButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
               // StartFrame sFrame = new StartFrame();
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });*/
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                miliSeconds = 0;
                msp.resetButtonHit();

                stopwatchLabel.setText("00:00");

                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        //Set this panels layout
        this.setLayout(new BorderLayout());
        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new GridLayout(2, 3));

        //Label settings
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setFont(new Font("Arial", Font.BOLD, 30));
        nameLabel.setOpaque(true);
        nameLabel.setBackground(Color.GREEN);
        nameLabel.setForeground(Color.WHITE);

        flagLabel.setHorizontalAlignment(JLabel.CENTER);
        flagLabel.setFont(new Font("Arial", Font.BOLD, 30));
        flagLabel.setOpaque(true);
        flagLabel.setBackground(Color.GREEN);
        flagLabel.setForeground(Color.WHITE);

        timer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (msp.gameStarted) {
                    String strTime = msp.getElapsedToString() + "";
                    
                    /**We should make the label display the time in 00:00 format**/
                    
      
                    stopwatchLabel.setText(strTime + "");
                    flagLabel.setText(msp.getNumberOfMinesLeft() + " Mines Left");
                }else{
                    stopwatchLabel.setText("0");
                }
            }
        });
        timer.start();

        //add labels to header panel
        JLabel emptyLabel1 = new JLabel("");
        emptyLabel1.setOpaque(true);
        emptyLabel1.setBackground(Color.GREEN);
        JLabel emptyLabel2 = new JLabel("");
        emptyLabel2.setOpaque(true);
        emptyLabel2.setBackground(Color.GREEN);

        headerPanel.add(emptyLabel1);
        headerPanel.add(nameLabel);
        headerPanel.add(emptyLabel2);
        headerPanel.add(stopwatchLabel);
        headerPanel.add(resetButton);
        headerPanel.add(flagLabel);

        //Bottom layout settings
        // thirdLayout.setLayout(new BorderLayout());
        /* switch (difficultyString){
         case "Easy":
         msp.setPreferredSize(new Dimension(600, 600));
         break;
         case "Medium":
         msp.setPreferredSize(new Dimension(800, 800));
         break;
         case "Hard":
         msp.setPreferredSize(new Dimension(1000, 1000));
         break;
         default:
         msp.setPreferredSize(new Dimension(600, 600));
         }*/
        //Set the layout for this panel
        //this.setLayout(borderLayout);
        /*  this.add(firstLayout, BorderLayout.NORTH);
         this.add(secondLayout, BorderLayout.CENTER);
         this.add(thirdLayout, BorderLayout.SOUTH);*/
        this.add(headerPanel, BorderLayout.NORTH);
        this.add(msp, BorderLayout.CENTER);

    }

}
