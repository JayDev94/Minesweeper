/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import com.sun.prism.paint.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;
import javax.swing.text.View;

/**
 *
 * @author Juan
 */
public class GridPanel extends JPanel implements ActionListener {

    int x = 20;
    int y = 20;
    //A 2D array of type buttons will hold the buttons
    //Button[][] jButtonGrid = new Button[x][y];
    JButton[][] jButtonGrid = new JButton[x][y];
    boolean[][] mine2DArray;
    BufferedImage flag = null;


    public GridPanel() {
        //draw image
        /*try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream input = classLoader.getResourceAsStream("/images/flag.png");
            flag = ImageIO.read(input);
        } catch (IOException ex) {
            Logger.getLogger(GridPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        //Create a boolean 2D array to hold the mines
        mine2DArray = new boolean[x][y];
        for (int i = 0; i < x; i++) {
            for (int k = 0; k < y; k++) {
                mine2DArray[i][k] = Math.random() < .1;
            }
        }

        //Initialize a new 10x10 gridlayout 
        setLayout(new GridLayout(x, y));

        //The first loop attains the x values
        //The second attains the y values
        //Inside the nested loop, create a button with the x, y, values and set a listener
        //the buttons are added to every single element in the jButtonGrid
        for (int i = 0; i < x; i++) {
            for (int k = 0; k < y; k++) {
                jButtonGrid[i][k] = new JButton();
                jButtonGrid[i][k].addActionListener(this);
                jButtonGrid[i][k].setBackground(java.awt.Color.WHITE);
                jButtonGrid[i][k].addMouseListener(new MouseAdapter() {
                    
                    @Override
                    public void mousePressed(MouseEvent m) {
                        if (m.getButton() == MouseEvent.BUTTON3) {
                            JButton flagButton = ((JButton) m.getSource());
                            
                            
                            
                            for (int i = 0; i < jButtonGrid.length; i++) {
                                for (int j = 0; j < jButtonGrid.length; j++) {
                                    if (jButtonGrid[i][j] == flagButton) {
                                        
                                        java.awt.Color flagColor = jButtonGrid[i][j].getBackground();
                                        java.awt.Color white = java.awt.Color.WHITE;
                                        java.awt.Color red = java.awt.Color.RED;
                                  
                                        
                                        if (flagColor.equals(white)){
                                            System.out.println("Background Color is white");
                                            jButtonGrid[i][j].setBackground(red);
                                        }else{
                                            jButtonGrid[i][j].setBackground(white);
                                        }
                                        
                                    }
                                }
                            }

                        }

                    }

                });
                add(jButtonGrid[i][k]);
            }
        }

    }

    //On button pressed event
    @Override
    public void actionPerformed(ActionEvent e
    ) {
        JButton selectedButton = ((JButton) e.getSource());
        //Here we will check to see if the isMine bool is true for a mine
        //showAllMines();
        //if the selected mine is a mine, show all mines
        //if it is a left click
        System.out.println("Went into actionPerformed Method");
        for (int i = 0; i < jButtonGrid.length; i++) {
            for (int j = 0; j < jButtonGrid.length; j++) {
                if (jButtonGrid[i][j] == selectedButton) {
                    //If selected mine is mine, show mines
                    if (mine2DArray[i][j] == true) {
                        showAllMines();
                    } else {
                        floodFill(i, j, new ArrayList<Point2D>());
                        //Display # of surrounding mines
                        // jButtonGrid[i][j].setText(surroundingMines(i, j) + "");
                        // jButtonGrid[i][j].setForeground(java.awt.Color.BLACK);
                        //grid[i][j].setBackground(java.awt.Color.GREEN);
                    }
                    System.out.println("The selected button's lo6cation is: " + i + ", " + j);
                }
            }
        }

        throw new UnsupportedOperationException(
                "Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    //All mines will be shown if their is a mine

    public void showAllMines() {
        //Iterate through array and check if location has mine, set texture/color if so
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if (mine2DArray[i][j] == true) {
                    jButtonGrid[i][j].setBackground(java.awt.Color.BLACK);
                }
            }
        }
    }

    //Check for surrounding mines and return the count
    public int surroundingMines(int x, int y) {
        int mineCounter = 0;
        System.out.println("Testing grid Cordinates:   x=" + x + " y=" + y);
        //Bottom left and inside the edge
        if (x - 1 >= 0 && y - 1 >= 0 && x - 1 < jButtonGrid.length && y - 1 < jButtonGrid.length) {
            if (mine2DArray[x - 1][y - 1]) {
                mineCounter++;
            }
        }
        //Bottom and inside the edge
        if (x >= 0 && y - 1 >= 0 && x < jButtonGrid.length && y - 1 < jButtonGrid.length) {
            if (mine2DArray[x][y - 1]) {
                mineCounter++;
            }
        }
        //Bottom right and inside the edge
        if (x + 1 >= 0 && y - 1 >= 0 && x + 1 < jButtonGrid.length && y - 1 < jButtonGrid.length) {
            if (mine2DArray[x + 1][y - 1]) {
                mineCounter++;
            }
        }
        //Left and inside the edge
        if (x - 1 >= 0 && y >= 0 && x - 1 < jButtonGrid.length && y < jButtonGrid.length) {
            if (mine2DArray[x - 1][y]) {
                mineCounter++;
            }
        }
        //Right and inside the edge
        if (x + 1 >= 0 && y >= 0 && x + 1 < jButtonGrid.length && y < jButtonGrid.length) {
            if (mine2DArray[x + 1][y]) {
                mineCounter++;
            }
        }
        //Top left and inside the edge
        if (x - 1 >= 0 && y + 1 >= 0 && x - 1 < jButtonGrid.length && y + 1 < jButtonGrid.length) {
            if (mine2DArray[x - 1][y + 1]) {
                mineCounter++;
            }
        }
        //Top and inside the edge
        if (x >= 0 && y + 1 >= 0 && x < jButtonGrid.length && y + 1 < jButtonGrid.length) {
            if (mine2DArray[x][y + 1]) {
                mineCounter++;
            }
        }
        //Top right and inside the edge
        if (x + 1 >= 0 && y + 1 >= 0 && x + 1 < jButtonGrid.length && y + 1 < jButtonGrid.length) {
            if (mine2DArray[x + 1][y + 1]) {
                mineCounter++;
            }
        }
        return mineCounter;
    }

    //Flood Fill algorithm
    public void floodFill(int x, int y, List<Point2D> checkedCord) {
        //Checks to see if the current corrdinates is contained as a Point2D object in the list
        //If it is, return
        //If not then add it to the checkedList

        if (isInGrid(x, y)) {
            Point2D p1 = new Point2D.Double(x, y);
            if (checkedCord.contains(p1)) {
                return;
            } else {
                //Differentiate the returning count by setting the color, texture, or something
                switch (surroundingMines(x, y)) {
                    case 0:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 1:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 2:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 3:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 4:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 5:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 6:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 7:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                        break;
                    case 8:
                        jButtonGrid[x][y].setBackground(java.awt.Color.ORANGE);
                }
                //Set the object's text to the returning count only if not = 0
                if (!(surroundingMines(x, y) == 0)) {
                    jButtonGrid[x][y].setText(surroundingMines(x, y) + "");
                }
                //if the surrounding mines count is > 0 return to block recursion 
                if (surroundingMines(x, y) > 0) {
                    return;
                } else {
                    /*Set the fill color or texture here*/
                    jButtonGrid[x][y].setBackground(java.awt.Color.GREEN);
                    //Add the object of cordinates to the list
                    checkedCord.add(p1);
                    //Implement recursion for left, right, top, and bottom values from cordinates
                    floodFill(x - 1, y, checkedCord);
                    floodFill(x + 1, y, checkedCord);
                    floodFill(x, y - 1, checkedCord);
                    floodFill(x, y + 1, checkedCord);
                }
            }
        }

    }

    //A quick method that returns true if values are inside the jButtonGrid
    public boolean isInGrid(int x, int y) {
        if (x >= 0 && y >= 0 && x < jButtonGrid.length && y < jButtonGrid.length) {
            return true;
        }
        return false;
    }


}
