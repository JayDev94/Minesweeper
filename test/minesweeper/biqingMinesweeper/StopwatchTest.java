/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pipposheng
 */
public class StopwatchTest {
    
    public StopwatchTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isRunning method, of class Stopwatch.
     */
    @Test
    public void testIsRunning() {
        System.out.println("isRunning");
        Stopwatch instance = new Stopwatch();
        instance.start();
        boolean result = instance.isRunning();
        assertEquals(true , result);
    
    }

  

    /**
     * Test of start method, of class Stopwatch.
     */
    @Test
    public void testStart() {
        System.out.println("start");
        Stopwatch instance = new Stopwatch();
        instance.start();
        assertEquals(true,instance.isRunning());
    }

    /**
     * Test of stop method, of class Stopwatch.
     */
    @Test
    public void testStop() {
        System.out.println("stop");
        Stopwatch instance = new Stopwatch();
        instance.start();
        instance.stop();
        assertEquals(false,instance.isRunning());

    }

    /**
     * Test of elapsed method, of class Stopwatch.
     */
    @Test
    public void testElapsed() {
        System.out.println("elapsed");
        Stopwatch instance = new Stopwatch();
        instance.start();
        
        instance.stop();
        long endTime = instance.getEndTime();
        long startTime = instance.getStartTime();
        
        long result = instance.elapsed();
        assertEquals(endTime-startTime, result);
  
    }

    /**
     * Test of toString method, of class Stopwatch.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Stopwatch instance = new Stopwatch();
        instance.start();
        instance.stop();
        String result = instance.toString();
        
        String expResult = Double.toString((double)instance.elapsed()/1000000000.0) + " Seconds";
        
        
        assertEquals(expResult,result);
    }
    
}
