/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Biqing Sheng
 */
public class MinesweeperPanelIT {
    
    public MinesweeperPanelIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    MinesweeperPanel instance = new MinesweeperPanel("easy");
    
    /**
     * Test of createRandomMines method, of class MinesweeperPanel.
     */
    @Test
    public void testCreateRandomMines() {
        System.out.println("createRandomMines");       
        int mineNumber = 0;
        final int MINE = 10;
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                if(instance.counts[x][y]==MINE) {
                mineNumber++;
                }
            }
        }
        assertEquals(mineNumber,MINE);
    }

    /**
     * Test of clearZeros method, of class MinesweeperPanel.
     */
    @Test
    public void testClearZeros() {
        System.out.println("clearZeros");
        ArrayList<Integer> toClear = new ArrayList<Integer>();
        instance.clearZeros(toClear);
        assertEquals(toClear.size(),0);
    }

    /**
     * Test of actionPerformed method, of class MinesweeperPanel.
     */
    @Test
    public void testActionPerformed(){
        System.out.println("actionPerformed");
    }

    /**
     * Test of lostGame method, of class MinesweeperPanel.
     */
    @Test
    public void testLostGame() {
        System.out.println("lostGame");
        instance.lostGame();
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {                
                    if (instance.counts[x][y] == 0) {
                        assertEquals(instance.buttons[x][y].getBackground(),Color.green);
                    } else if (instance.counts[x][y] < 10 && instance.counts[x][y] > 0) {
                        assertEquals(instance.buttons[x][y].getBackground(),Color.orange);
                    } else {
                        assertEquals(instance.buttons[x][y].getBackground(),Color.black);
                    }              
            }
        }
    }

    /**
     * Test of checkWin method, of class MinesweeperPanel.
     */
    @Test
    public void testCheckWin() {
        System.out.println("checkWin");
        instance.checkWin();
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                if (instance.counts[x][y] != 10 && instance.buttons[x][y].isEnabled() == true) {
                    assertEquals(instance.won,false);
                }
            }
        }
    }

    /**
     * Test of mouseClicked method, of class MinesweeperPanel.
     */
    @Test
    public void testMouseClicked() {
        System.out.println("mouseClicked");
    }

    /**
     * Test of resetButtonHit method, of class MinesweeperPanel.
     */
    @Test
    public void testResetButtonHit() {
        System.out.println("resetButtonHit");
        instance.resetButtonHit();
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                assertEquals(instance.buttons[x][y].isEnabled(),true);
            }
        }
    }

    /**
     * Test of gameHasEnded method, of class MinesweeperPanel.
     */
    @Test
    public void testGameHasEnded() {
        System.out.println("gameHasEnded");
        boolean expResult = false;
        boolean result = instance.gameHasEnded();
        assertEquals(expResult, result);
    }

    /**
     * Test of gameStarted method, of class MinesweeperPanel.
     */
    @Test
    public void testGameStarted() {
        System.out.println("gameStarted");
        boolean expResult = false;
        boolean result = instance.gameStarted();
        assertEquals(expResult, result);
    }

    /**
     * Test of getElapsedToString method, of class MinesweeperPanel.
     */
    @Test
    public void testGetElapsedToString() {
        System.out.println("getElapsedToString");    
        String expResult = Integer.toString((int)(instance.stopwatch.elapsed()/1000000000));
        String result = instance.getElapsedToString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberOfMinesLeft method, of class MinesweeperPanel.
     */
    @Test
    public void testGetNumberOfMinesLeft() {
        System.out.println("getNumberOfMinesLeft");
        instance.minesLeft = 3;
        String expResult = "3";
        String result = instance.getNumberOfMinesLeft();
        assertEquals(expResult, result);
    }   
    
}
