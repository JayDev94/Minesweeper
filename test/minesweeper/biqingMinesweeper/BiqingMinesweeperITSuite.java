/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author pipposheng
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({minesweeper.biqingMinesweeper.StopwatchIT.class, minesweeper.biqingMinesweeper.StartFrameIT.class, minesweeper.biqingMinesweeper.MinesweeperPanelIT.class, minesweeper.biqingMinesweeper.StartPanelIT.class})
public class BiqingMinesweeperITSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
