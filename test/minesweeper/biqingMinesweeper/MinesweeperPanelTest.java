/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper.biqingMinesweeper;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pipposheng
 */
public class MinesweeperPanelTest {
    
    public MinesweeperPanelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createRandomMines method, of class MinesweeperPanel.
     */
    @Test
    public void testCreateRandomMines() {
        System.out.println("createRandomMines");
        MinesweeperPanel instance = new MinesweeperPanel("easy");
        int mineNumber = 0;
        final int MINE = 10;
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                if(instance.counts[x][y]==MINE) {
                mineNumber++;
                }
            }
        }
        assertEquals(mineNumber,MINE);
    }

    /**
     * Test of clearZeros method, of class MinesweeperPanel.
     */
    @Test
    public void testClearZeros() {
        System.out.println("clearZeros");
        ArrayList<Integer> toClear = new ArrayList<Integer>();
        MinesweeperPanel instanceOne = new MinesweeperPanel("easy");
        instanceOne.clearZeros(toClear);
        assertEquals(toClear.size(),0);
    }

    /**
     * Test of actionPerformed method, of class MinesweeperPanel.
     */
    @Test
    public void testActionPerformed(){
        System.out.println("actionPerformed");
    }

    /**
     * Test of lostGame method, of class MinesweeperPanel.
     */
    @Test
    public void testLostGame() {
        System.out.println("lostGame");
        MinesweeperPanel instanceTwo = new MinesweeperPanel("easy");
        instanceTwo.lostGame();
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {                
                    if (instanceTwo.counts[x][y] == 0) {
                        assertEquals(instanceTwo.buttons[x][y].getBackground(),Color.green);
                    } else if (instanceTwo.counts[x][y] < 10 && instanceTwo.counts[x][y] > 0) {
                        assertEquals(instanceTwo.buttons[x][y].getBackground(),Color.orange);
                    } else {
                        assertEquals(instanceTwo.buttons[x][y].getBackground(),Color.black);
                    }              
            }
        }
    }

    /**
     * Test of checkWin method, of class MinesweeperPanel.
     */
    @Test
    public void testCheckWin() {
        System.out.println("checkWin");
        MinesweeperPanel instanceThree = new MinesweeperPanel("easy");
        instanceThree.checkWin();
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                if (instanceThree.counts[x][y] != 10 && instanceThree.buttons[x][y].isEnabled() == true) {
                    assertEquals(instanceThree.won,false);
                }
            }
        }
    }

    /**
     * Test of mouseClicked method, of class MinesweeperPanel.
     */
    @Test
    public void testMouseClicked() {
        System.out.println("mouseClicked");
    }

    /**
     * Test of resetButtonHit method, of class MinesweeperPanel.
     */
    @Test
    public void testResetButtonHit() {
        System.out.println("resetButtonHit");
        MinesweeperPanel instance = new MinesweeperPanel("easy");
        instance.resetButtonHit();
        for (int x = 0; x < 10; x++) {
            for (int y = 0; y < 10; y++) {
                assertEquals(instance.buttons[x][y].isEnabled(),true);
            }
        }
    }

    /**
     * Test of gameHasEnded method, of class MinesweeperPanel.
     */
    @Test
    public void testGameHasEnded() {
        System.out.println("gameHasEnded");
        MinesweeperPanel instance = new MinesweeperPanel("easy");
        boolean expResult = false;
        boolean result = instance.gameHasEnded();
        assertEquals(expResult, result);
    }

    /**
     * Test of gameStarted method, of class MinesweeperPanel.
     */
    @Test
    public void testGameStarted() {
        System.out.println("gameStarted");
        MinesweeperPanel instance = new MinesweeperPanel("easy");
        boolean expResult = false;
        boolean result = instance.gameStarted();
        assertEquals(expResult, result);
    }

    /**
     * Test of getElapsedToString method, of class MinesweeperPanel.
     */
    @Test
    public void testGetElapsedToString() {
        System.out.println("getElapsedToString");
        MinesweeperPanel instance = new MinesweeperPanel("easy");      
        String expResult = Integer.toString((int)(instance.stopwatch.elapsed()/1000000000));
        String result = instance.getElapsedToString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberOfMinesLeft method, of class MinesweeperPanel.
     */
    @Test
    public void testGetNumberOfMinesLeft() {
        System.out.println("getNumberOfMinesLeft");
        MinesweeperPanel instance = new MinesweeperPanel("easy");
        instance.minesLeft = 3;
        String expResult = "3";
        String result = instance.getNumberOfMinesLeft();
        assertEquals(expResult, result);
    }
}
